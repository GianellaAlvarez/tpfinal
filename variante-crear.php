<?php
include "header.php";
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Variantes</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item active">Variantes</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- right column -->
        <div class="col-md-12">
          <!-- general form elements disabled -->
          <div class="card card-blue">
            <div class="card-header">
              <h3 class="card-title">Variantes</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <form id="variante" action="variante.php" method="post" name="variante-form">
                <div class="card-body">
                  <div class="form-group">
                    <label for="varName">Variante</label>
                    <input type="text" name="name" class="form-control" id="varianteName" placeholder="Ingrese el nombre de la variante">
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <input type="hidden" name="variante-form" value="agregar">
                  <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
              </form>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
  <?php
  include "footer.php";
  $file = basename($_SERVER['PHP_SELF']);
  include "scripts/script-$file";
  ?>

  </body>

  </html>