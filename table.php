<?php 
    include "header.php";
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Alumnos Listar</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Alumnos Listar</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">DataTable with default features</h3>
              </div>
              <?php 
                $alumnos = array(
                  array(
                    "nombre" => "Pablo",
                    "apellido" => "Ingino",
                    "dni" => "30654538",
                    "carrera" => "Analista de sistemas",
                    "anio" => "Tercero"
                  ),
                  array (
                    "nombre" => "Pepe",
                    "apellido" => "Lopez",
                    "dni" => "34589985",
                    "carrera" => "Analista de sistemas",
                    "anio" => "Tercero"
                  ),
                  array (
                    "nombre" => "Florencia",
                    "apellido" => "Perez",
                    "dni" => "36895256",
                    "carrera" => "Analista de sistemas",
                    "anio" => "Segundo"
                  ),
                )
              
              
              ?>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>DNI</th>
                    <th>Carrera</th>
                    <th>Año</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php 
                        foreach($alumnos as $alumno) {
                          echo "<tr>";
                          echo "<td>".$alumno["nombre"]."</td>";
                          echo "<td>".$alumno["apellido"]."</td>";
                          echo "<td>".$alumno["dni"]."</td>";
                          echo "<td>".$alumno["carrera"]."</td>";
                          echo "<td>".$alumno["anio"]."</td>";
                          echo "</tr>";
                        }
                    ?>
                  </tbody>
                  <tfoot>
                  <tr>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>DNI</th>
                    <th>Carrera</th>
                    <th>Año</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
<?php 
    include "footer.php";
    $file = basename($_SERVER['PHP_SELF']);
    echo $file;
    include "scripts/script-$file";
?>
</body>
</html>
