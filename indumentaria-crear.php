<?php
include "header.php";
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Indumentaria</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item active">Indumentaria</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- right column -->
        <div class="col-md-12">
          <!-- general form elements disabled -->
          <div class="card card-blue">
            <div class="card-header">
              <h3 class="card-title">Indumentaria</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <form id="indumentaria" action="indumentaria.php" method="post" name="indumentaria-form">
                <div class="row">
                  <div class="form-group col-md-6 ">
                    <label for="indumentariaName">Nombre</label>
                    <input type="text" name="name" class="form-control" id="indumentariaName" placeholder="Ingrese el nombre de la indumentaria">
                  </div>
                  <div class="form-group col-md-6 ">
                    <label for="indumentariaPrice">Precio</label>
                    <input type="number" name="precio" class="form-control" id="indumentariaPrice" placeholder="Ingrese el precio">
                  </div>
                  
                  <div class="form-group col-md-6 ">
                    <label for="course">Talle</label>
                    <select class="form-control select2bs4" style="width: 100%;" name="talle">
                      <?php
                      try {
                        include_once 'config/db.php';
                        $stmt = "SELECT * FROM talles";
                        $resultado = $conn->query($stmt);
                      } catch (Exception $e) {
                        $error = $e->getMessage();
                        echo $error;
                      }
                      while ($talle = $resultado->fetch_assoc()) {
                      ?>
                        <option value="<?php echo $talle['nombre_talle']; ?>"><?php echo $talle['nombre_talle']; ?></option>

                      <?php } ?>
                    </select>
                  </div>

                  <div class="form-group col-md-6 ">
                    <label for="course">Variante</label>
                    <select class="form-control select2bs4" style="width: 100%;" name="variante">
                      <?php

                      try {
                        include_once 'config/db.php';
                        $stmt = "SELECT * FROM variantes";
                        $resultado = $conn->query($stmt);
                      } catch (Exception $e) {
                        $error = $e->getMessage();
                        echo $error;
                      }
                      while ($variante = $resultado->fetch_assoc()) {
                      ?>
                        <option value="<?php echo $variante['nombre']; ?>"><?php echo $variante['nombre']; ?></option>

                      <?php } ?>
                    </select>
                  </div>

                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <input type="hidden" name="indumentaria-form" value="add">
                  <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
              </form>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
  <?php
  include "footer.php";
  $file = basename($_SERVER['PHP_SELF']);
  include "scripts/script-$file";
  ?>

  </body>

  </html>