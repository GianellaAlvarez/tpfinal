<?php

if ($_POST['envio-form'] == 'add') {
    $name = $_POST['name'];
    $direccion = $_POST['direccion'];
    $pedido = $_POST['pedido'];
    $cantidad = $_POST['cantidad'];
  
   

    

    try {
        include_once 'config/db.php';
        $stmt = $conn->prepare("INSERT INTO envios (cantidad, nombre, direccion, indumentaria) VALUES (?, ?, ?, ?)");
        $stmt->bind_param('isss', $cantidad, $name, $direccion, $pedido);
        $stmt->execute();

        $id_insertado = $stmt->insert_id;
        $errno = $stmt->errno;
        if ($stmt->affected_rows && $errno === 0) {
            $respuesta = array(
                'respuesta' => 'exitoso',
                'id' => $id_insertado
            );
        } elseif ($errno === 1406) {
            $respuesta = array(
                'respuesta' => 'error',
                'errno' => $errno,
                'error' => $stmt->error,
            );
        } elseif ($errno === 1062) {
            $respuesta = array(
                'respuesta' => 'error',
                'errno' => $errno,
                'error' => $stmt->error,
            );
        }
        $stmt->close();
        $conn->close();
    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
    }
    die(json_encode($respuesta));

    
}else if ($_POST['envio-form'] == 'edit') {
    $id = $_POST['id'];
    $enviado = 1;
    

    try {
        include_once 'config/db.php';
        $stmt = $conn->prepare("UPDATE envios SET enviado = ? WHERE id_envio = ?;");
        $stmt->bind_param('ii', $enviado, $id);
        $stmt->execute();

        $errno = $stmt->errno;

        if ($stmt->affected_rows && $errno === 0) {
            $respuesta = array(
                'respuesta' => 'exitoso'
            );
        } elseif ($errno === 1406) {
            $respuesta = array(
                'respuesta' => 'error',
                'errno' => $errno,
                'error' => $stmt->error,
            );
        } elseif ($errno === 1062) {
            $respuesta = array(
                'respuesta' => 'error',
                'errno' => $errno,
                'error' => $stmt->error,
            );
        }
        $stmt->close();
        $conn->close();
    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
    }
    die(json_encode($respuesta));
}

