<?php

if (isset($_POST['login-form'])) {
    $usuario = $_POST['email'];
    $password = $_POST['pass'];

    try {
        include_once 'config/db.php';
        $dat = $conn->prepare("SELECT * FROM usuario WHERE email = ?;");
        $dat->bind_param('s', $usuario);
        $dat->execute();
        $dat->bind_result($id, $email, $pass, $firstName, $lastName, $role, $avatars);
        $errno = $dat->errno;
        $error = $dat->error;
        if ($dat->affected_rows) {
            $existe = $dat->fetch();
            if ($existe) {
                if ($password === $pass) {
                    session_start();
                    $_SESSION['email'] = $email;
                    $_SESSION['id'] = $id;
                    $_SESSION['firstName'] = $firstName;
                    $_SESSION['lastName'] = $lastName;
                    $_SESSION['role'] = $role;
                    $_SESSION['avatar'] = $avatars;

                    $respuesta = array(
                        'respuesta' => 'exitoso',
                        'firstName' => $firstName,
                        'email' => $email
                    );
                } else {
                    $respuesta = array(
                        'respuesta' => 'error_pass'
                    );
                }
            } else {
                $respuesta = array(
                    'respuesta' => 'error',
                    'email' => $usuario
                );
            }
        }
    } catch (Exeption $e) {
        echo "Error: " . $e->getMessage();
    }

    die(json_encode($respuesta));
}
