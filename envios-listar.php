<?php
include "header.php";
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Lista de Envios</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Listar Envios</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Lista de envios</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Nombre Cliente</th>
                    <th>Dirección</th>
                    <th>Pedido</th>
                    <th>Cantidad</th>
                    <th>Estado</th>
                    <th>Acciones</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  try {
                    include_once 'config/db.php';
                    $stmt = "SELECT * FROM envios";
                    $resultado = $conn->query($stmt);
                  } catch (Exception $e) {
                    $error = $e->getMessage();
                    echo $error;
                  }
                  while ($user = $resultado->fetch_assoc()) {

                  ?>
                    <tr>
                      <td><?php echo $user['id_envio']; ?></td>
                      <td><?php echo $user['nombre']; ?></td>
                      <td><?php echo $user['direccion']; ?></td>
                      <td><?php echo $user['indumentaria']; ?></td>
                      <td><?php echo $user['cantidad']; ?></td>
                      <td><?php if($user['enviado'] == 1) { 
                        echo "Pedido despachado";
                      }else {
                        echo "Pendiente de envio";
                      } ?></td>
                      <td>
                      <?php if($user['enviado'] != 1) { ?>
                        <a href="#" data-id="<?php echo $user['id_envio']; ?>" class="btn bg-blue bnt-flat margin edit"><i class="fas fa-arrow-circle-right"></i></a>
                        <?php
                        } ?>
                      </td>
                    </tr>
                  <?php
                  } ?>
                </tbody>
                <tfoot>
                  <tr>
                    <th>ID</th>
                    <th>Nombre Cliente</th>
                    <th>Dirección</th>
                    <th>Pedido</th>
                    <th>Cantidad</th>
                    <th>Estado</th>
                    <th>Acciones</th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
  <?php
  include "footer.php";
  $file = basename($_SERVER['PHP_SELF']);
  include "scripts/script-$file";
  ?>
  </body>

  </html>