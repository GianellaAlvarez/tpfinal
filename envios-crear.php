<?php
include "header.php";
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Nuevo Envio</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item active">Envios</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- right column -->
        <div class="col-md-12">
          <!-- general form elements disabled -->
          <div class="card card-blue">
            <div class="card-header">
              <h3 class="card-title">Envios</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <form id="envio" action="envios.php" method="post" name="envio-form">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Nombre de cliente</label>
                      <input type="text" class="form-control" placeholder="Ingrese nombre del cliente" name="name">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Dirección</label>
                      <input type="text" class="form-control" placeholder="Ingrese una dirección" name="direccion">
                    </div>
                  </div>
                  <div class="form-group col-md-6 ">
                    <label for="course">Pedido</label>
                    <select class="form-control select2bs4" style="width: 100%;" name="pedido">
                      <?php

                      try {
                        include_once 'config/db.php';
                        $stmt = "SELECT * FROM indumentarias";
                        $resultado = $conn->query($stmt);
                      } catch (Exception $e) {
                        $error = $e->getMessage();
                        echo $error;
                      }
                      while ($ind = $resultado->fetch_assoc()) {
                      ?>
                        <option value="<?php echo $ind['nombre']; ?>"><?php echo $ind['nombre']; ?></option>

                      <?php } ?>
                    </select>
                  </div>
                  <div class="col-sm-3">
                    <div class="form-group">
                      <label>Cantidad</label>
                      <input type="number" class="form-control" name="cantidad">
                    </div>
                  </div>

                
                
                </div>
                <div class="card-footer">
                  
                  <input type="hidden" name="envio-form" value="add">
                  <button type="submit" class="btn btn-primary">Guardar</button>
                </div>

              </form>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
  <?php
  include "footer.php";
  $file = basename($_SERVER['PHP_SELF']);
  include "scripts/script-$file";
  ?>
  </body>

  </html>