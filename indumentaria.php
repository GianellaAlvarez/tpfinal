<?php

if ($_POST['indumentaria-form'] == 'add') {
    $name = $_POST['name'];
    $precio = $_POST['precio'];
    $talle = $_POST['talle'];
    $variante = $_POST['variante'];
   
    try {
        include_once 'config/db.php';
        $stmt = $conn->prepare("INSERT INTO indumentarias (nombre, precio, talle, variante) VALUES (?, ?, ?, ?)");
        $stmt->bind_param('siss', $name, $precio, $talle, $variante);
        $stmt->execute();

        $id_insertado = $stmt->insert_id;
        $errno = $stmt->errno;
        /* 
        $stmt = $conn->prepare("INSERT INTO courses_has_matters (courses_id_career, matters_id_matter) VALUES (?, ?)");
        $stmt->bind_param('ii', $id_course, $id_insertado);
        $stmt->execute();*/

        if ($stmt->affected_rows && $errno === 0) {
            $respuesta = array(
                'respuesta' => 'exitoso',
                'id' => $id_insertado
            );
        } elseif ($errno === 1406) {
            $respuesta = array(
                'respuesta' => 'error',
                'errno' => $errno,
                'error' => $stmt->error,
            );
        } elseif ($errno === 1062) {
            $respuesta = array(
                'respuesta' => 'error',
                'errno' => $errno,
                'error' => $stmt->error,
            );
        }
        $stmt->close();
        $conn->close();
    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
    }
    die(json_encode($respuesta));
}
if ($_POST['indumentaria-form'] == 'delete') {
    $id = $_POST['id'];

    try {
        include_once 'config/db.php';
        $stmt = $conn->prepare('DELETE FROM indumentarias WHERE id_indumentaria = ? ');
        $stmt->bind_param('i', $id);
        $stmt->execute();
        if ($stmt->affected_rows) {
            $respuesta = array(
                'respuesta' => 'exito',
                'id_eliminado' => $id
            );
        } else {
            $respuesta = array(
                'respuesta' => 'error'
            );
        }
    } catch (Exception $e) {
        $respuesta = array(
            'respuesta' => $e->getMessage()
        );
    }
    die(json_encode($respuesta));
}