<?php
include "header.php";
$id = $_GET["id"];
try {
  include_once 'config/db.php';
  $stmt = "SELECT * FROM users INNER JOIN students ON users.id_users = students.users_id_users INNER JOIN students_has_courses ON students_has_courses.students_id_students = students.id_students INNER JOIN courses ON courses.id_course = students_has_courses.courses_id_course WHERE id_users=$id;";
  $student = $conn->query($stmt);
} catch (Exception $e) {
  $error = $e->getMessage();
  echo $error;
}
$datos = $student->fetch_assoc();
$id_student = intval($datos["students_id_students"]);
try {
  include_once 'config/db.php';
  $stmt = "SELECT * FROM students_has_matters INNER JOIN matters ON students_has_matters. matters_id_matters = matters.id_matters WHERE students_id_students = $id_student ";
  $matters = $conn->query($stmt);
} catch (Exception $e) {
  $error = $e->getMessage();
  echo $error;
}
$matter_data = $matters->fetch_assoc();
?>




<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>General Form</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item active">Alumnos</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- right column -->
        <div class="col-md-12">
          <!-- general form elements disabled -->
          <div class="card card-blue">
            <div class="card-header">
              <h3 class="card-title">Alumno</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <form id="student" action="student.php" method="post" name="student-form">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Nombre</label>
                      <input type="text" class="form-control" placeholder="Nombre del Alumno" name="name" value="<?php echo $datos['user_name']; ?>">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Apellido</label>
                      <input type="text" class="form-control" placeholder="Apellido del Alumno" name="lastname" value="<?php echo $datos['user_lastname']; ?>">
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      <label>Email</label>
                      <input type="email" class="form-control" placeholder="Email del Alumno" name="email" value="<?php echo $datos['user_email']; ?>">
                    </div>
                  </div>
                  <div class="col-sm-2">
                    <div class="form-group">
                      <label>DNI</label>
                      <input type="text" class="form-control" placeholder="DNI del Alumno" name="dni" value="<?php echo $datos['user_dni']; ?>">
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="form-group">
                      <label>Password</label>
                      <input type="password" class="form-control" placeholder="Password del Alumno" name="pass" id="pass">
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="form-group">
                      <label>Confirme el Password</label>
                      <input type="password" class="form-control" placeholder="Reingrese el Password del Alumno" name="pass_again">
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Avatar</label>
                      <input type="file" class="form-control" name="avatar" id="avatar" value="<?php echo $datos['avatar']; ?>">
                    </div>
                  </div>

                </div>
            </div>
            <div class="card-footer">
              <input type="hidden" name="id_user" value="<?php echo $id; ?>">
              <input type="hidden" name="id_student" value="<?php echo $id_student; ?>">
              <input type="hidden" name="student-form" value="edit">
              <button type="submit" class="btn btn-primary">Guardar</button>
            </div>

            </form>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!--/.col (right) -->
    </div>
    <!-- /.row -->
</div><!-- /.container-fluid -->
</section>
<!-- /.content -->
<?php
include "footer.php";
$file = basename($_SERVER['PHP_SELF']);
include "scripts/script-$file";
?>
</body>

</html>