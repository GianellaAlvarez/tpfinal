<?php
include "header.php";
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Variantes</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Variantes</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Tabla de listado de Variantes</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Variante</th>
                    <th>Acciones</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  try {
                    include_once 'config/db.php';
                    $stmt = "SELECT * FROM variantes";
                    $resultado = $conn->query($stmt);
                  } catch (Exception $e) {
                    $error = $e->getMessage();
                    echo $error;
                  }
                  while ($variante = $resultado->fetch_assoc()) {
                  ?>
                    <tr>
                      <td><?php echo $variante['id']; ?></td>
                      <td><?php echo $variante['nombre']; ?></td>
                      <td>
                        <!-- Si el usuario es distinto de rol 1 no puede ejecutar acciones -->
                        <?php if ($_SESSION['role'] == '1') { ?>
                          <a href="#" data-id="<?php echo $variante['id']; ?>" class="btn bg-maroon bnt-flat margin delete"><i class="fas fa-eraser"></i></a>
                        <?php } ?>

                      </td>
                    </tr>
                  <?php } ?>
                </tbody>
                <tfoot>
                  <tr>
                    <th>ID</th>
                    <th>Variante</th>
                    <th>Acciones</th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
  <?php
  include "footer.php";
  $file = basename($_SERVER['PHP_SELF']);
  echo $file;
  include "scripts/script-$file";
  ?>
  </body>

  </html>